package com.valmikitech.twiliootpexample;

import com.twilio.Twilio;
import com.valmikitech.twiliootpexample.config.TwilioConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class TwilioOtpExampleApplication {

	@Autowired
	private TwilioConfig twilioConfig;

    @PostConstruct
	public void initTwilio(){
      Twilio.init(twilioConfig.getAccountSid(), twilioConfig.getAuthToken());
	}

	public static void main(String[] args) {
		SpringApplication.run(TwilioOtpExampleApplication.class, args);
	}

}
