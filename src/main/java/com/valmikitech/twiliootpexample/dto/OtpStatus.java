package com.valmikitech.twiliootpexample.dto;

public enum OtpStatus {

    DELIVERED, FAILED
}
